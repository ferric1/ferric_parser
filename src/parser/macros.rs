//! This module contains macros that assist in parsing Ferric code
//! into an abstract syntax tree (AST).


/// # parse_func_call!
///
/// Parses a function call, whether built-in or user-defined.
///
/// # Parameters
///
/// - `$self`: The parser instance.
///
/// # Example
///
/// The macro is designed for internal use. In the parser, it can be invoked as:
///
/// ```ignore
/// parse_func_call!(self);
/// ```
macro_rules! parse_func_call {
    ($self:ident) => {{
        let current_token = $self.tokens[$self.position].clone(); // Cache the token
        $self.position += 1;
        let args = $self.parse_function_arguments()?;
        match current_token {
            Token::BuiltInFunction(func_name) => Ok(ASTNode::BuiltInFunctionCall {
                name: func_name,
                args,
            }),
            Token::Identifier(func_name) => Ok(ASTNode::UserFunctionCall {
                name: func_name,
                args,
            }),
            _ => Err(format!(
                "Expected a function name, found {:?}",
                current_token
            )),
        }
    }};
}

/// # parse_operator!
///
/// Parses an operator from the token stream.
///
/// # Parameters
///
/// - `$self`: The parser instance.
#[allow(unused_macros)]
macro_rules! parse_operator {
    ($self:ident) => {
        match &$self.tokens[$self.position] {
            Token::Operator(op) => {
                $self.position += 1;
                Ok(*op)
            }
            _ => Err("Expected an operator".to_string()),
        }
    };
}

/// # parse_keyword!
///
/// Parses a keyword from the token stream.
///
/// # Parameters
///
/// - `$self`: The parser instance.
macro_rules! parse_keyword {
    ($self:ident) => {
        match &$self.tokens[$self.position] {
            Token::Keyword(kw) => {
                $self.position += 1;
                Ok(kw)
            }
            _ => Err("Expected a keyword".to_string()),
        }
    };
}

/// # parse_identifier!
///
/// Parses an identifier from the token stream.
///
/// # Parameters
///
/// - `$self`: The parser instance.
macro_rules! parse_identifier {
    ($self:ident) => {
        match &$self.tokens[$self.position] {
            Token::Identifier(id) => {
                $self.position += 1;
                Ok(ASTNode::Variable(id.clone()))
            }
            _ => Err("Expected an identifier".to_string()),
        }
    };
}

/// # parse_number!
///
/// Parses a number from the token stream.
///
/// # Parameters
///
/// - `$self`: The parser instance.
#[allow(unused_macros)]
macro_rules! parse_number {
    ($self:ident) => {
        match &$self.tokens[$self.position] {
            Token::Number(n) => {
                $self.position += 1;
                Ok(ASTNode::Number(*n))
            }
            _ => Err("Expected a number".to_string()),
        }
    };
}

/// # expect_token!
///
/// Checks the current token against an expected variant and returns an AST node if it matches.
///
/// The macro supports two patterns:
/// 1. Matching a token that contains a value.
/// 2. Matching a token that doesn't have an associated value.
///
/// # Parameters
///
/// - `$self`: The parser instance.
/// - `$token_variant`: The expected variant of the `Token` enum.
/// - `$val`: (Optional) The value associated with the token.
/// - `$ast_node_variant`: The variant of the `ASTNode` enum to return if the token matches.
///
/// # Usage Examples
///
/// 1. **Expecting a Token with a Value**
///
///    ```ignore
///    expect_token!(self, Identifier(name) => Variable);
///    ```
///
/// 2. **Expecting a Token without a Value**
///
///    ```ignore
///    expect_token!(self, OpenParen => OpenParenthesis);
///    ```
macro_rules! expect_token {
    ($self:ident, $token_variant:ident($val:ident) => $ast_node_variant:ident) => {
        match &$self.tokens[$self.position] {
            Token::$token_variant(ref $val) => {
                $self.position += 1;
                Ok(ASTNode::$ast_node_variant($val.clone()))
            }
            _ => Err(format!("Expected a {}", stringify!($token_variant))),
        }
    };
    ($self:ident, $token_variant:ident => $ast_node_variant:ident) => {
        match $self.tokens[$self.position] {
            Token::$token_variant => {
                $self.position += 1;
                Ok(ASTNode::$ast_node_variant)
            }
            _ => Err(format!("Expected a {}", stringify!($token_variant))),
        }
    };
}

/// # parse_variable_declaration!
///
/// Parses a variable declaration of the form `let var_name = expression;`.
///
/// # Parameters
///
/// - `$self`: The parser instance.
///
/// # Example
///
/// For Ferric code like:
///
/// ```ferric
/// let x = 5;
/// ```
///
/// The macro would parse the declaration and produce an AST representation.
macro_rules! parse_variable_declaration {
    ($self:ident) => {
        if let Ok(Keyword::Let) = parse_keyword!($self) {
            if let Ok(ASTNode::Variable(name)) = parse_identifier!($self) {
                if let Some(Token::Assign) = $self.peek_next_token() {
                    $self.position += 1; // Consume Assign
                    let expr = $self.expression()?;

                    // Check if expr is a constant and update the symbol table
                    match &expr {
                        ASTNode::Number(n) => $self.symtable.set(&name, *n),
                        _ => {
                            if $self.symtable.contains(&name) {
                                $self.symtable.remove(&name);
                            }
                        }
                    }

                    Ok(ASTNode::Assign {
                        name,
                        value: Box::new(expr),
                    })
                } else {
                    Ok(ASTNode::Variable(name))
                }
            } else {
                Err("Expected an identifier after 'let'".to_string())
            }
        } else {
            Err("Expected 'let' keyword".to_string())
        }
    };
}

/// # parse_if_statement!
///
/// Parses an `if` statement with potential `else if` and `else` branches.
///
/// # Parameters
///
/// - `$self`: The parser instance.
///
/// # Example
///
/// For Ferric code like:
///
/// ```ferric
/// if (x < 5) {
///     print("less than 5");
/// } else if (x == 5) {
///     print("equal to 5");
/// } else {
///     print("greater than 5");
/// }
/// ```
///
/// The macro would parse the conditional branches and produce an AST representation.
macro_rules! parse_if_statement {
    ($self:ident) => {
        if let Ok(Keyword::If) = parse_keyword!($self) {
            let mut branches = Vec::new();
            let condition = $self.expression()?;
            if let Some(Token::OpenBrace) = $self.peek_next_token() {
                $self.position += 1; // Consume OpenBrace
                let body = $self.parse_block()?;
                branches.push((Box::new(condition), Box::new(body)));

                let mut else_body: Option<Box<ASTNode>> = None;

                loop {
                    match $self.peek_next_token() {
                        Some(Token::Keyword(Keyword::ElseIf)) => {
                            $self.position += 1; // Consume ElseIf
                            let else_if_condition = $self.expression()?;
                            if let Some(Token::OpenBrace) = $self.peek_next_token() {
                                $self.position += 1; // Consume OpenBrace
                                let else_if_body = $self.parse_block()?;
                                branches
                                    .push((Box::new(else_if_condition), Box::new(else_if_body)));
                            } else {
                                return Err("Expected '{' after else if condition".to_string());
                            }
                        }
                        Some(Token::Keyword(Keyword::Else)) => {
                            $self.position += 1; // Consume Else
                            if let Some(Token::OpenBrace) = $self.peek_next_token() {
                                $self.position += 1; // Consume OpenBrace
                                let else_block = $self.parse_block()?;
                                else_body = Some(Box::new(else_block));
                                break;
                            } else {
                                return Err("Expected '{' after 'else'".to_string());
                            }
                        }
                        _ => break,
                    }
                }

                Ok(ASTNode::Conditional {
                    branches,
                    else_body,
                })
            } else {
                Err("Expected '{' after if condition".to_string())
            }
        } else {
            Err("Expected 'if' keyword".to_string())
        }
    };
}

/// # parse_while_loop
///
/// Parses a `while` loop.
///
/// # Parameters
///
/// - `$self`: The parser instance.
///
/// # Example
///
/// For Ferric code like:
///
/// ```ferric
/// while (x < 10) {
///     x = x + 1;
/// }
/// ```
///
/// The macro would parse the loop and produce an AST representation.
macro_rules! parse_while_loop {
    ($self:ident) => {
        if let Ok(Keyword::While) = parse_keyword!($self) {
            let condition = $self.expression()?;
            if let Some(Token::OpenBrace) = $self.peek_next_token() {
                $self.position += 1; // Consume OpenBrace
                let body = $self.parse_block()?;
                Ok(ASTNode::While {
                    condition: Box::new(condition),
                    body: Box::new(body),
                })
            } else {
                Err("Expected '{' after while condition".to_string())
            }
        } else {
            Err("Expected 'while' keyword".to_string())
        }
    };
}
