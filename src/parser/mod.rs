// Module: parser
// Purpose: parsing the tokens into an AST
// Path: src/parser/mod.rs
#[macro_use]
mod macros;

pub mod parser;

pub mod code_gen;
pub mod symbol_table;

pub use self::parser::Parser;
pub use symbol_table::SymbolTable;
pub use code_gen::ast_node::ASTNode;

#[cfg(test)]
mod tests;

