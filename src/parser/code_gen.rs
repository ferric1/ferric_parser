// path: src/parser/code_gen.rs
// module: code_gen
// purpose: module and later library for the code generation from the ferric language to both assembly and bash

pub mod ast_node;
#[macro_use]
pub mod macros;
#[macro_use]
pub mod arithmetic;
#[macro_use]
pub mod data_movement;
#[macro_use]
pub mod system_calls;
#[macro_use]
pub mod string_operations;
#[macro_use]
pub mod system_functions;
#[macro_use]
pub mod declarations;



