// path: src/parser/code_gen/arithmetic.rs
// module: arithmetic
// purpose: storing all macros related to arithmetic operations in x86-64 NASM assembly.


#[cfg(feature = "asm")]
/// Module containing macros related to arithmetic operations in x86-64 NASM assembly.

/// # add!
///
/// Adds source to destination.
///
/// # Parameters
///
/// - `$dest`: The destination operand.
/// - `$src`: The source operand.
macro_rules! add {
    ($dest:expr, $src:expr) => {
        format!("ADD {}, {}", $dest, $src)
    };
}

/// # sub!
///
/// Subtracts source from destination.
///
/// # Parameters
///
/// - `$dest`: The destination operand.
/// - `$src`: The source operand.
macro_rules! sub {
    ($dest:expr, $src:expr) => {
        format!("SUB {}, {}", $dest, $src)
    };
}

/// # mul!
///
/// Multiplies the accumulator (RAX) by the source.
///
/// # Parameters
///
/// - `$src`: The source operand.
macro_rules! mul {
    ($src:expr) => {
        format!("MUL {}", $src)
    };
}

/// # div!
///
/// Divides the accumulator (RAX) by the source.
///
/// # Parameters
///
/// - `$src`: The source operand.
macro_rules! div {
    ($src:expr) => {
        format!("DIV {}", $src)
    };
}

/// # inc!
///
/// Increments the destination.
///
/// # Parameters
///
/// - `$dest`: The operand to be incremented.
macro_rules! inc {
    ($dest:expr) => {
        format!("INC {}", $dest)
    };
}

/// # dec!
///
/// Decrements the destination.
///
/// # Parameters
///
/// - `$dest`: The operand to be decremented.
macro_rules! dec {
    ($dest:expr) => {
        format!("DEC {}", $dest)
    };
}

