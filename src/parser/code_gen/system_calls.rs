// path: src/parser/code_gen/system_calls.rs
// module: system_calls
// purpose: storing all macros related to system calls and their structure in x86-64 NASM assembly.

#[cfg(feature = "asm")]
/// Module containing macros related to system calls in x86-64 NASM assembly.

/// # syscall!
///
/// Invokes a system call using the specified system call number.
///
/// # Parameters
///
/// - `$num`: The system call number.
macro_rules! syscall {
    ($num:expr) => {
        concat!(
            mov!("RAX", $num),
            "\nSYSCALL"
        )
    };
}

/// # syscall_with_args!
///
/// Invokes a system call with arguments.
///
/// # Parameters
///
/// - `$num`: The system call number.
/// - `$args`: The arguments to pass to the system call (in the order RDI, RSI, RDX, etc.).
macro_rules! syscall_with_args {
    ($num:expr, $($args:expr),*) => {
        {
            let mut code = mov!("RAX", $num).to_string();
            let registers = ["RDI", "RSI", "RDX", "R10", "R8", "R9"];
            for (reg, arg) in registers.iter().zip([$($args),*].iter()) {
                code.push_str(&format!("\n{}", mov!(reg, arg)));
            }
            code.push_str("\nSYSCALL");
            code
        }
    };
}
