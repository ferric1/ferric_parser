// path: src/parser/code_gen/system_functions.rs
// module: system_functions
// purpose: storing all macros related to system functions in x86-64 NASM assembly.



#[cfg(feature = "asm")]

/// # print_to_console!
///
/// Prints a string to the console using the write syscall.
///
/// # Parameters
///
/// - `$str_ptr`: The pointer to the string.
/// - `$len`: The length of the string.
///
/// # Example
///
/// ```rust,ignore
/// print_to_console!("my_string_ptr", "11");
/// ```
///
/// Expected NASM ELF64 output:
///
/// ```assembly
/// MOV rax, 1
/// MOV rdi, 1
/// MOV rsi, my_string_ptr
/// MOV rdx, 11
/// SYSCALL
/// ```
macro_rules! print_to_console {
    ($str_ptr:expr, $len:expr) => {
        format!("MOV rax, 1\\nMOV rdi, 1\\nMOV rsi, {}\\nMOV rdx, {}\\nSYSCALL", $str_ptr, $len)
    };
}
