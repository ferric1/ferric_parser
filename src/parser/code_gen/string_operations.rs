// path: src/parser/code_gen/string_operations.rs
// module: string_operations
// purpose: storing all macros related to string operations in x86-64 NASM assembly.

#[cfg(feature = "asm")]
/// Module containing macros related to string operations in x86-64 NASM assembly.

/// # stos!
///
/// Stores the value in the accumulator register to the location pointed to by the DI/RDI.
///
/// # Example
///
/// ```rust,ignore
/// stos!(qword);
/// ```
///
/// Expected NASM ELF64 output:
///
/// ```assembly
/// STOSQ
/// ```
macro_rules! stos {
    () => {
        "STOSB"
    };
    (word) => {
        "STOSW"
    };
    (dword) => {
        "STOSD"
    };
    (qword) => {
        "STOSQ"
    };
}

/// # movs!
///
/// Moves data from the location pointed to by the SI/RSI to the location pointed to by the DI/RDI.
///
/// # Example
///
/// ```rust,ignore
/// movs!(dword);
/// ```
///
/// Expected NASM ELF64 output:
///
/// ```assembly
/// MOVSD
/// ```
macro_rules! movs {
    () => {
        "MOVSB"
    };
    (word) => {
        "MOVSW"
    };
    (dword) => {
        "MOVSD"
    };
    (qword) => {
        "MOVSQ"
    };
}

/// # cmps!
///
/// Compares data at the location pointed to by the SI/RSI with the data at the location pointed to by the DI/RDI.
///
/// # Example
///
/// ```rust,ignore
/// cmps!();
/// ```
///
/// Expected NASM ELF64 output:
///
/// ```assembly
/// CMPSB
/// ```
macro_rules! cmps {
    () => {
        "CMPSB"
    };
    (word) => {
        "CMPSW"
    };
    (dword) => {
        "CMPSD"
    };
    (qword) => {
        "CMPSQ"
    };
}

