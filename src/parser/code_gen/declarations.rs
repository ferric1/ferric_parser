// path: src/parser/code_gen/declarations.rs
// module: declarations
// purpose: storing all macros related to declarations in x86-64 NASM assembly.

#[macro_use]
#[cfg(feature = "asm")]
/// Module containing macros related to variable declarations in x86-64 NASM assembly.

/// # declare_stack_rsp!
///
/// Declares a variable on the stack using RSP offsets.
///
/// # Parameters
///
/// - `$size`: The size of the variable in bytes.
///
/// # Examples
///
/// ```ignore
/// let asm_code = declare_stack_rsp!("8");
/// ```
///
/// This generates the assembly:
/// ```assembly
/// SUB RSP, 8
/// ```
macro_rules! declare_stack_rsp {
    ($size:expr) => {
        sub!("RSP", $size)
    };
}

/// # declare_stack_rbp!
///
/// Declares a variable on the stack using RBP frame.
/// Assumes RBP has been set previously.
///
/// # Parameters
///
/// - `$size`: The size of the variable in bytes.
///
/// # Examples
///
/// ```ignore
/// let asm_code = declare_stack_rbp!("8");
/// ```
///
/// This generates the assembly:
/// ```assembly
/// SUB RSP, 8
/// ```
macro_rules! declare_stack_rbp {
    ($size:expr) => {
        sub!("RSP", $size)
    };
}

/// # declare_stack_frame_rbp!
///
/// Begins a new RBP-based stack frame and allocates space for local variables.
///
/// # Parameters
///
/// - `$size`: The size of the variables in the frame in bytes.
///
/// # Examples
///
/// ```ignore
/// let asm_code = declare_stack_frame_rbp!("40");
/// ```
///
/// This generates the assembly:
/// ```assembly
/// PUSH RBP
/// MOV RBP, RSP
/// SUB RSP, 40
/// ```
macro_rules! declare_stack_frame_rbp {
    ($size:expr) => {
        concat!(
            push!("RBP"),
            mov!("RBP", "RSP"),
            sub!("RSP", $size)
        )
    };
}

/// # end_stack_frame_rbp!
///
/// Ends an RBP-based stack frame, deallocating local variables and restoring RBP.
///
/// # Examples
///
/// ```ignore
/// let asm_code = end_stack_frame_rbp!();
/// ```
///
/// This generates the assembly:
/// ```assembly
/// MOV RSP, RBP
/// POP RBP
/// ```
macro_rules! end_stack_frame_rbp {
    () => {
        concat!(
            mov!("RSP", "RBP"),
            pop!("RBP")
        )
    };
}

/// # declare_heap!
///
/// Declares a variable on the heap.
///
/// # Parameters
///
/// - `$size`: The size of the variable in bytes.
///
/// # Examples
///
/// ```ignore
/// let asm_code = declare_heap!("32");
/// ```
///
/// This generates the assembly for an `mmap` syscall to allocate 32 bytes.
///
/// It looks something like this:
///
/// ```assembly
/// MOV RAX, 9
/// MOV RDI, 0
/// MOV RSI, 32
/// MOV RDX, 7
/// MOV R10, 34
/// MOV R8, -1
/// MOV R9, 0
/// SYSCALL
/// ```
macro_rules! declare_heap {
    ($size:expr) => {
        syscall_with_args!("9", "0", $size, "7", "34", "-1", "0")
    };
}

#[cfg(test)]
mod test {
    #[test]
    fn test() {
        let tmp: String = declare_heap!("32");
        dbg!(tmp);
    }
}
