use ferric_lexer::{BuiltInFunction, Operator};

#[derive(PartialEq, Debug, Clone)]
pub enum ASTNode {
    Program(Vec<ASTNode>),
    Statement(Box<ASTNode>),
    BinaryOp {
        op: Operator,
        left: Box<ASTNode>,
        right: Box<ASTNode>,
    },
    Assign {
        name: String,
        value: Box<ASTNode>,
    },
    Variable(String),
    Number(i32),
    StringLiteral(String),
    While {
        condition: Box<ASTNode>,
        body: Box<ASTNode>,
    },
    BuiltInFunctionCall {
        name: BuiltInFunction,
        args: Vec<ASTNode>,
    },
    UserFunctionCall {
        name: String,
        args: Vec<ASTNode>,
    },
    Conditional {
        branches: Vec<(Box<ASTNode>, Box<ASTNode>)>, // (condition, body)
        else_body: Option<Box<ASTNode>>,
    },
    // Add other control structures as needed
}

impl ASTNode {
    #[cfg(feature = "bash")]
    pub fn to_bash(&self) -> String {
        match self {
            ASTNode::Program(nodes) => nodes
                .iter()
                .map(|node| node.to_bash())
                .collect::<Vec<_>>()
                .join("\n"),
            ASTNode::Statement(node) => node.to_bash(),
            ASTNode::BinaryOp { op, left, right } => {
                format!(
                    "$(( {} {} {} ))",
                    left.to_bash(),
                    op.to_bash(),
                    right.to_bash()
                )
            }
            ASTNode::Assign { name, value } => match **value {
                ASTNode::BuiltInFunctionCall {
                    name: BuiltInFunction::Exec,
                    ref args,
                } => match &args[0] {
                    ASTNode::StringLiteral(s) => format!("{}=$({})", name, s),
                    ASTNode::Variable(var_name) => format!("{}=$({})", name, var_name),
                    _ => panic!("Invalid argument for Exec command!"),
                },
                _ => format!("{}={}", name, value.to_bash()),
            },
            ASTNode::Variable(name) => name.clone(),
            ASTNode::Number(n) => n.to_string(),
            ASTNode::While { condition, body } => {
                format!(
                    "while (( {} ));\ndo\n{}\ndone",
                    condition.to_bash(),
                    body.to_bash()
                )
            }
            ASTNode::BuiltInFunctionCall { name, args } => {
                match name {
                    BuiltInFunction::Exec => match &args[0] {
                        ASTNode::StringLiteral(s) => s.clone(),
                        ASTNode::Variable(var_name) => {
                            format!("${}", var_name)
                        }
                        _ => panic!("Invalid argument for Exec command!"),
                    },
                    BuiltInFunction::Print => match &args[0] {
                        ASTNode::StringLiteral(s) => format!("echo {}", s),
                        ASTNode::Variable(name) => format!("echo ${}", name),
                        _ => format!("echo {}", args[0].to_bash()),
                    },
                    // ... (handle other functions as needed)
                    _ => format!(
                        "{} {}",
                        name.to_bash(),
                        args.iter()
                            .map(|arg| arg.to_bash())
                            .collect::<Vec<_>>()
                            .join(" ")
                    ),
                }
            }
            ASTNode::UserFunctionCall { name, args } => {
                let args_bash = args
                    .iter()
                    .map(|arg| arg.to_bash())
                    .collect::<Vec<_>>()
                    .join(" ");
                format!("{} {}", name, args_bash)
            }
            ASTNode::StringLiteral(s) => format!("\"{}\"", s),
            ASTNode::Conditional {
                branches,
                else_body,
            } => {
                let mut bash_code = String::new();

                for (index, (condition, body)) in branches.iter().enumerate() {
                    match index {
                        0 => {
                            bash_code += &format!(
                                "if (( {} )); then\n{}\n",
                                condition.to_bash(),
                                body.to_bash()
                            )
                        } // Handle `if`
                        _ => {
                            bash_code += &format!(
                                "elif (( {} )); then\n{}\n",
                                condition.to_bash(),
                                body.to_bash()
                            )
                        } // Handle `elif`
                    }
                }

                if let Some(else_node) = else_body {
                    bash_code += &format!("else\n{}\n", else_node.to_bash());
                }

                bash_code + "fi"
            } // Handle other cases as needed
        }
    }

    #[cfg(feature = "asm")]
    pub fn to_asm(&self) -> String {
        "hi".to_string()
    }
}
