// path: src/parser/code_gen/data_movement.rs
// module: data_movement
// purpose: storing all macros related to data movement in x86-64 NASM assembly.


#[cfg(feature = "asm")]
/// Module containing macros related to data movement in x86-64 NASM assembly.

/// # mov!
///
/// Moves data from src to dest
///
/// # Parameters
///
/// - `$dest`: The destination of the data.
/// - `$src`: The source of the data
macro_rules! mov {
    ($dest:expr, $src:expr) => {
        format!("MOV {}, {}", $dest, $src)
    };
}

/// # push!
///
/// Pushes value to the stack
///
/// # Parameters
///
/// - `$value`: The value to be pushed (reg or mem).
macro_rules! push {
    ($value:expr) => {
        format!("PUSH {}", $value)
    };
}

/// # pop!
///
/// Pops value to the stack
///
/// # Parameters
///
/// - `$value`: The value to be popped (reg or mem).
macro_rules! pop {
    ($value:expr) => {
        format!("POP {}", $value)
    };
}

/// # lea!
///
/// Loads effective address of a memory operand into a register.
///
/// # Parameters
///
/// - `$reg`: The register to load into.
/// - `$mem`: The memory to load
macro_rules! lea {
    ($reg:expr, $mem:expr) => {
        format!("LEA {}, {}", $reg, $mem)
    };
}
