use std::collections::HashMap;

#[derive(Default, Debug)]
pub struct SymbolTable {
    table: HashMap<String, i32>,
}

impl SymbolTable {
    pub fn new() -> Self {
        SymbolTable {
            table: HashMap::new(),
        }
    }

    pub fn remove(&mut self, key: &String) {
        self.table.remove(key);
    }

    pub fn set(&mut self, name: &str, value: i32) {
        self.table.insert(name.to_string(), value);
    }

    pub fn get(&self, name: &str) -> Option<i32> {
        self.table.get(name).cloned()
    }

    pub fn contains(&self, name: &str) -> bool {
        self.table.contains_key(name)
    }
}
