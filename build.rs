fn main() {
    let binding = std::env::var("CARGO_FEATURES")
        .unwrap_or_default();

    // Access the features that are enabled
    let features: Vec<&str> = binding.split(',').collect();

    // Check if both `bash` and `asm` features are enabled
    if features.contains(&"bash") && features.contains(&"asm") {
        panic!("The features `bash` and `asm` are mutually exclusive and cannot be enabled at the same time.");
    }
}