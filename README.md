
# ferric_parser

`ferric_parser` is a Rust library designed to parse and optimize abstract syntax trees for the Ferric programming language.
Note that it requires you to use the `ferric_lexer` to work properly as it assumes that the tokens it receives are from my lexer, 
this is so that I may develop everything on my own and not rely on external crates.

## Installation

Include `ferric_parser` and `ferric_lexer` in your `Cargo.toml`:

```toml
[dependencies]
ferric_parser = "0.1.0"
ferric_lexer = "0.1.0"
```

Then run:

```
cargo build
```

## Usage

```rust
use ferric_lexer::Lexer;
use ferric_parser::Parser;

let code = "your Ferric code here";
let tokens = Lexer::new(&code).lex();

let parser = Parser::new(code);
let ast = parser.parse().unwrap();
```

## Compiling

Currently, the project as a whole only compiles to bash, but it will be expanded in the future with support to compile to assembly.

The parser is responsible for the code gen at the moment but this will be moved to its own repo soon so that we can compile only what we need to.

## Contributing

1. Fork the repository.
2. Create a new branch for your changes.
3. Implement your feature or bug fix.
4. Add, commit, and push your changes to your branch.
5. Submit a pull request.

## License

This project is licensed under the MIT License.

---

For further details, refer to the LICENSE file.
